
generateData <- function(nCols, nRows) {
    t <- list()
    multFactor <- 1000


    createIntColumn = function() as.integer(runif(nRows) *  multFactor)
    createFloatColumn = function() runif(nRows) * multFactor


    t[[1]] = createIntColumn()
    t[[2]] = createIntColumn()
    t[[3]] = createIntColumn()
    t[[4]] = createIntColumn()



    startColumn = 5

    for (i in startColumn:nCols) {

        if (i %% 2 == 0)
            dataCol = createIntColumn()
        else
            dataCol =  createFloatColumn()
        # complex(real=runif(nRows), imaginary=runif(nRows))
        t[[i]] = dataCol

    }

    myData <<- t

}


f <- function(colIndex, t) {

    dataCol <- t[[colIndex]]

    res <- 0

    for (i in 1:length(dataCol)) {
        res <- res + dataCol[[i]]
    }
    res


}
rir.compile(f)
rir.markFunction(f, DisableInline=TRUE)



columnwiseSum  <- function(t) {

    res <- c()

    for (i in 1L:cols) {
        
        duration <- system.time({
          res[[i]] <- f(i, t)
          
        })[[1]]
        
        line <- paste(type, "", typeof(t[[i]]), i-1, duration, sep=",")
        write(line, file = fullFilePath,
            append = TRUE)
        
        
    }

    res

}


cols = 50L
rows  = 10000000L

generateData(cols, rows)


execute <- function(ignore) {

    fullFilePath <<- commandArgs(trailing=T)[1]
    type <<- commandArgs(trailing=T)[2]
    #cat("experiment, event, run,i s \n")
    columnwiseSum(myData)

}


execute()
