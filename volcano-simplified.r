

bench_rays <- function(sun.angle, nr, nc) {
    shadow <- matrix(1, ncol = nc, nrow = nr)
    sunangle <- sun.angle / 180 * pi
    angle <- -90 / 180 * pi
    diffangle <- 90 / 180 * pi
    numberangles <- 25
    anglebreaks <- seq(angle, diffangle, length.out = numberangles)
    maxdistance <- floor(sqrt(nc^2 + nr^2))
    sinsun <- sin(sunangle)
    cossun <- cos(sunangle)

    for (i in 1:nr) {
        for (j in 1:nc) {
            for (anglei in anglebreaks) {
                for (k in 1:maxdistance) {
                    xcoord <- i + sinsun * k
                    ycoord <- j + cossun * k

                    if (xcoord > nr || ycoord > nc || xcoord < 0 || ycoord < 0)
                        break

                    tanangheight <- volcano[i, j] + tan(anglei) * k

                    if (tanangheight < volcano[max(1, floor(xcoord)), max(1, floor(ycoord))]) {
                        shadow[i, j] <- shadow[i, j] - 1 / numberangles
                        break
                    }
                }
            }
        }
    }

    shadow
}

nr <- dim(volcano)[[1]]
nc <- dim(volcano)[[2]]

for (i in 1:5) {
    start <- Sys.time()
    bench_rays(181, nr, nc)
    end <- Sys.time()
    taken <- end - start
    cat(i,",",taken, "\n")
}

a <- attributes(volcano)
volcano <- as.integer(volcano)
attributes(volcano) <- a

for (i in 1:5) {
    start <- Sys.time()
    bench_rays(181, nr, nc)
    end <- Sys.time()
    taken <- end - start
    cat(i+5,",",taken, "\n")
}
