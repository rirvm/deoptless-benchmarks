library(ggplot2)
library(reshape2)
library(viridis)
library(shiny)

cast_rays <- function(height.map, sun.angle, n.angles) {
  nc <- ncol(height.map)
  nr <- nrow(height.map)
  shadow <- matrix(1, ncol = nc, nrow = nr)
  sunangle <- sun.angle / 180 * pi
  angle <- -90 / 180 * pi
  diffangle <- 90 / 180 * pi
  anglebreaks <- sapply(seq(angle, diffangle, length.out = n.angles), tan)
  maxdistance <- floor(sqrt(nc^2 + nr^2))
  sinsun <- sin(sunangle)
  cossun <- cos(sunangle)
  for (i in 1:nr) {
    for (j in 1:nc) {
      for (anglei in anglebreaks) {
        for (k in 1:maxdistance) {
          xcoord <- i + sinsun * k
          ycoord <- j + cossun * k
          if (xcoord > nr ||
              ycoord > nc ||
              xcoord < 0 || ycoord < 0) break
          tanangheight <- height.map[i, j] + anglei * k
          if (all(c(height.map[ceiling(xcoord), ceiling(ycoord)],
                    height.map[floor(xcoord),   ceiling(ycoord)],
                    height.map[ceiling(xcoord), floor(ycoord)],
                    height.map[floor(xcoord),   floor(ycoord)]) < tanangheight)) next
          if (tanangheight < interpolate_surface(height.map, xcoord, ycoord)) {
            shadow[i, j] <- shadow[i, j] - 1 / length(anglebreaks)
            break
          }
        }
      }
    }
  }
  shadow
}

bilinear <- function(data, x, y) {
  i <- max(1, floor(x))
  j <- max(1, floor(y))
  XT <- (x - i)
  YT <- (y - j)
  result <- (1 - YT) * (1 - XT) * data[i, j]
  nx <- nrow(data)
  ny <- ncol(data)
  if (i + 1 <= nx) {
    result <- result + (1 - YT) * XT * data[i + 1, j]
  }
  if (j + 1 <= ny) {
    result <- result + YT * (1 - XT) * data[i, j + 1]
  }
  if (i + 1 <= nx && j + 1 <= ny) {
    result <- result + YT * XT * data[i + 1, j + 1]
  }
  result
}

bilinear2 <- function(data, x, y) {
  i <- max(1, floor(x))
  j <- max(1, floor(y))
  XT <- (x - i)
  YT <- (y - j)
  result <- YT * XT * data[i, j]
  nx <- nrow(data)
  ny <- ncol(data)
  if (i + 1 <= nx) {
    result <- result + YT * (1 - XT) * data[i + 1, j]
  }
  if (j + 1 <= ny) {
    result <- result + (1 - YT) * XT * data[i, j + 1]
  }
  if (i + 1 <= nx && j + 1 <= ny) {
    result <- result + (1 - YT) * (1 - XT) * data[i + 1, j + 1]
  }
  result
}

flat <- function(data, x, y) {
  i <- max(1, floor(x))
  j <- max(1, floor(y))
  data[i, j]
}

interpolate_surface <- bilinear

int_data <- (function() {
    readRDS("perlin-map.rds")

##   d <- c(174, 122)
##   data <- ambient::noise_perlin(d)
##   data <- data + min(data)
##   data <- as.integer(data * 100 + 100)
##   dim(data) <- d
##   data

##   data <- volcano
##   a <- attributes(data)
##   v <- as.integer(data)
##   attributes(v) <- a
##   v
})()

smooth_data <- (function() {
  s <- function(m) {
    res <- m
    for (i in 2:(nrow(m)-1))
      for (j in 2:(ncol(m)-1))
        res[i, j] <- mean(c(m[i, j-1], m[i, j], m[i, j+1],
                            m[i-1, j-1], m[i-1, j], m[i-1, j+1],
                            m[i+1, j-1], m[i+1, j], m[i+1, j+1]))
      res
  }

  v <- s(int_data)
  v2 <- s(v)
  v3 <- s(v2)
  v3
})()

ui <- fluidPage(
  title = "Cast Shadows",
  titlePanel(title = h1("Cast Shadows")),
  sidebarPanel(
    selectInput("angle", "Sun angle:",
                c("181", "30", "96", "200", "323"),
                selected = "181"),
    selectInput("rays", "Rays:",
                c("9", "5", "16", "25", "40"),
                selected = "9"),
    selectInput("fun", "Method:",
                c("bilinear", "bilinear2", "flat"),
                selected = "bilinear"),
    checkboxInput("smooth", "Smooth map", value = FALSE, width = NULL)),
  mainPanel(plotOutput("result"))
)

server <- function(input, output) {

    compute <- reactive({
        angle <- as.integer(input$angle)
        rays <- as.integer(input$rays)
        fun <- input$fun
        smooth <- input$smooth
        interpolate_surface <<- get(fun, globalenv())
        heights <- if (smooth) smooth_data else int_data
        cat("angle = ", angle, ", ",
            "rays = ", rays, ", ",
            "fun = ", fun, ", ",
            "smooth = ", smooth, "... ", sep = "")
        start <- Sys.time()
        res <- cast_rays(heights, angle, rays)
        end <- Sys.time()
        taken <- end - start
        cat(taken, " ", attributes(taken)$units, "\n", sep = "")
        res
    })

    output$result <- renderPlot({
        data <- compute()
        cat("rendering... ", sep = "")
        start <- Sys.time()
        res <- ggplot() +
            geom_tile(data = melt(data), aes(x = Var1, y = Var2, fill = value)) +
            scale_x_continuous("X coord", expand = c(0, 0)) +
            scale_y_continuous("Y coord", expand = c(0, 0)) +
            scale_fill_viridis() +
            theme_void() +
            theme(legend.position = "none")
        end <- Sys.time()
        taken <- end - start
        cat("took ", taken, " ", attributes(taken)$units, "\n", sep = "")
        res
    }, height = 600, width = 900)

}

shinyApp(ui = ui, server = server)
