
generateData <- function(nCols, nRows) {
    t <- list()
    multFactor <- 1000


    createIntColumn = function() as.integer(runif(nRows) *  multFactor)
    createFloatColumn = function() runif(nRows) * multFactor


    t[[1]] = createIntColumn()
    t[[2]] = createIntColumn()
    t[[3]] = createIntColumn()
    t[[4]] = createIntColumn()



    startColumn = 5

    for (i in startColumn:nCols) {

        if (i %% 2 == 1)
            dataCol = createIntColumn()
        else
            dataCol =  createFloatColumn()
        # complex(real=runif(nRows), imaginary=runif(nRows))
        t[[i]] = dataCol

    }

    myData <<- t

}


f <- function(colIndex, t) {

    dataCol <- t[[colIndex]]

    res <- 0

    for (i in 1:length(dataCol)) {
        res <- res + dataCol[[i]]
    }
    res


}
rir.compile(f)
rir.markFunction(f, DisableInline=TRUE)



columnwiseSum  <- function(t) {

    res <- c()

    for (i in 1L:cols) {
        res[[i]] <- f(i, t)
    }

    res

}


cols = 50L
rows  = 10000000L

generateData(cols, rows)


execute <- function(ignore) {
    print(columnwiseSum(myData))
}
